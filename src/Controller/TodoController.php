<?php

namespace App\Controller;

use App\Entity\Debilidades;
use App\Entity\Todo;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\User;
use App\Form\TodoFormType;
use Doctrine\ORM\Mapping\Id;

class TodoController extends AbstractController
{
    /**
     * @Route("/home", name="home")
     */
    public function home(){
        return $this-> render("Home/home.html.twig");
    }

    /**
     * @Route("/todo/{id}", name="getTodo")
     */
    public function getTodo($id, EntityManagerInterface $doctrine)
    {
        // $id=2;
        $repo = $doctrine->getRepository(User::class);
        $user=$repo->find($id);
        
        $repo = $doctrine->getRepository(Todo::class);
        $todo=$repo->findBy(['codeUser'=>$user]);
        return $this-> render("Todo/TodoBase.html.twig",["todo" => $todo]);
    }


    /**
     * @Route("/todo/createTodo", name="createTodo")
     */
    public function createTodo(Request $request, EntityManagerInterface $doctrine)
    {
        $form = $this->createForm(TodoFormType::class);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $todo = $form->getData();
            $user=$this->getUser();
            $todo->setCodeUser($user);
            $doctrine->persist($todo);
            $doctrine->flush();
            $id=$user->getId();
            $this->addFlash('success', 'Tarea creada correctamente');
            return $this->redirectToRoute('getTodo',['id'=>$id]);
        }

        return $this->render('Todo/createTodo.html.twig', ['Form' => $form->createView()]);
    }

    /**
     * @Route("/todo/updateTodo/{id}", name="updateTodo")
     */
    public function updateTodo(Todo $todo, Request $request, EntityManagerInterface $doctrine)
    {
        $form = $this->createForm(TodoFormType::class, $todo);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $todo = $form->getData();
            $user=$this->getUser();
            $doctrine->persist($todo);
            $doctrine->flush();
            $id=$user->getId();
            $this->addFlash('success', 'Tarea modificada correctamente');
            return $this->redirectToRoute('getTodo',['id'=>$id]);
        }

        return $this->render('Todo/createTodo.html.twig', ['Form' => $form->createView()]);
    }

    /**
     * @Route("/todo/doneTodo/{id}", name="doneTodo")
     */
    public function doneTodo(Todo $todo, Request $request, EntityManagerInterface $doctrine)
    {
        $todo->setDone(true);
        $doctrine->persist($todo);
        $doctrine->flush();
        $user=$this->getUser();
        $id=$user->getId();
        return $this->redirectToRoute("getTodo",['id'=>$id]);
    }

    /**
     * @Route("/todo/deltodo/{id}", name="deleteTodo")
     */
    public function deleteTodo(Todo $todo, EntityManagerInterface $doctrine)
    {
        $doctrine->remove($todo);
        $doctrine->flush();
        $user=$this->getUser();
        $id=$user->getId();
        return $this->redirectToRoute("getTodo",['id'=>$id]);
    }
   
}
