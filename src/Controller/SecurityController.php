<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class SecurityController extends AbstractController
{
    /**
     * @Route("/", name="app_login")
     */
    public function login(AuthenticationUtils $authenticationUtils): Response
    {
        if ($this->getUser()) {
            return $this->redirectToRoute('home');
        }

        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('security/login.html.twig', ['last_username' => $lastUsername, 'error' => $error]);
    }

    /**
     * @Route("/logout", name="app_logout")
     */
    public function logout(): void
    {
        throw new \LogicException('This method can be blank - it will be intercepted by the logout key on your firewall.');
    }

    /**
     * @Route("/newuser", name="app_register")
     */
    public function newUser(EntityManagerInterface $doctrine, UserPasswordEncoderInterface $encoder, Request $request)
    {
        if($request->request->get('email')){
            $user = new User();
            $user->setEmail($request->request->get('email'));
            $pass1=$request->request->get('password');
            $pass2=$request->request->get('password2');
            $user->setPassword($encoder->encodePassword($user,$pass1));
            
            $doctrine->persist($user);
            $doctrine->flush();
            
           
        }
        

        // $admin = new User();
        // $admin->setEmail("esther@gmail.com");
        // $admin->setRoles(['ROLE_USER', 'ROLE_ADMIN']);
        // $admin->setPassword($encoder->encodePassword($admin, '1234'));



        

        return $this->render('Register/Register.html.twig');
    }
}
